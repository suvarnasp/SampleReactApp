import { injectGlobal } from 'styled-components';

/* eslint no-unused-expressions: 0 */
injectGlobal`
  html,
  body {
    width: 100%;
    height: 100%;
  }

  body {
    background: linear-gradient(#eee, #aaa, #c2c2c2, #9b9b9b);
    font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
  }

  body.fontLoaded {
    font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif;
  }

  #app {
    min-height: 100%;
    min-width: 100%;
  }

  .app-content {
    max-width: 1024px;
    margin: 0 auto;
    padding: 10px;
  }

  p,
  label {
    font-family: Georgia, Times, 'Times New Roman', serif;
    line-height: 1.5em;
  }

  .widget{
    background: linear-gradient(#aaa, #c2c2c2, #9b9b9b);
    margin: 50px auto;
    padding: 5% 3%;
    max-width: 450px;
    height: 500px;
  }

  .sub-text{
    font-size: 20px;
    text-align: center;
    margin: 3%;
  }

`;
