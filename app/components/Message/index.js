import styled from 'styled-components';

const Message = styled.div`
  text-align: center;
  padding: 10px;
  color: green;
  font-weight: bold;
  font-size: 18px;
`;

export default Message;
