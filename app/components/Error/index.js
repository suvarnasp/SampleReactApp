import styled from 'styled-components';

const Error = styled.div`
  text-align: center;
  padding: 10px;
  color: #c43232;
  font-weight: bold;
  font-size: 18px;
`;

export default Error;
