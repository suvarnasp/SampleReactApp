import styled from 'styled-components';

export const Polyline = styled.polyline`
  stroke: #94caff;
  stroke-width: 5;
  fill:none;
  user-select: none;
`;

export const Circle = styled.circle`
  stroke: #0099CC;
  stroke-width: 4;
  fill: #b3b3b3;
  opacity: 0.7;
  user-select: none;

  &.selected {
    stroke: #0099CC;
  	fill: #00b6f3;
  	opacity: 1;
  }
  g:hover & {
  	opacity: 1;
  	cursor:pointer;
  }
`;

export const Dot = styled.circle`
  fill: #eee;
  opacity: 0.8;
  user-select: none;

  g:hover & {
  	cursor:pointer;
  }
`;
