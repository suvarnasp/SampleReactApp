/**
 * DB
 * @type {Object}
 */
let data = {
	// Default pattern
	pattern: [6,3,0,4,2,5,8]
};

module.exports = {
	get: (key) => {
		return data[key];
	},
	set: (key, value) => {
		data[key] = value;
	}
}
