const db = require('../db');
const isValidPattern = require('../../app/utils/validator').isPatternValid;

module.exports = function (req, res) {
	if (req.session.isLoggedIn) {
		let pattern = req.body && req.body.pattern;
		let isLoggedIn = req.session && req.session.isLoggedIn;
		pattern = pattern || [];

		if (isValidPattern(pattern)) {
			db.set('pattern', pattern);
			req.session.isLoggedIn = false;
	    res.json({ message: "Pattern is successfully changed" });
		} else {
	    	res.json({ errorMessage: "Invalid pattern" });
		}
	} else {
		res.json({ errorMessage: "You are not logged in" });
	}

}
