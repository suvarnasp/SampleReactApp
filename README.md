# Example of pattern locked application based on React, Redux

Application allows to unlock yourself and change lock pattern. Default lock pattern is 'M' shape i.e 6-3-0-4-2-5-8 (The pattern has numbers 0,1,2,3,4,5,6,7,8)


# Setup
1) Run commands
> git clone git@gitlab.com:suvarnasp/SampleReactApp.git

> cd SampleReactApp

> npm install

> npm run start

When server will be started, open application on http://localhost:3000/

